//
//  StarPatternViewController.swift
//  ProgressBar
//
//  Created by Akkarin Phain on 30/4/2564 BE.
//

import UIKit

class StarPatternViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        textLabel.text = "Please add your \n input in TextField"
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 5
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.cornerRadius = 5
    }

    @IBAction func ButtonAction(_ sender: UIButton) {
        guard let text = textField.text else { return }
        showStarPattern(input: Int(text) ?? 0)
    }
    

    private func showStarPattern(input: Int) {
        let treeHeight = input
        let treeWidth = treeHeight * 2 - 1

        for lineNumber in 1...treeHeight {

            let stars = 2 * lineNumber - 1
            var line = ""

            let spaces = (treeWidth - stars) / 2
            if spaces > 0 {
                line = String(repeating: " ", count: spaces)
            }
            
            if lineNumber != treeHeight {
                for n in 1...stars {
                    if n == 1 || n / stars == 1 {
                        line += "x"
                    } else if n / stars != 1 {
                        line += " "
                    }
                }
            } else {
                line += String(repeating: "x", count: stars)
            }
            print (line)
            textLabel.text = "Please check in your console"
        }
    }
    

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
      let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
      tap.cancelsTouchesInView = false
      view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
      view.endEditing(true)
    }
}
