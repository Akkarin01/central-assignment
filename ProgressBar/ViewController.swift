//
//  ViewController.swift
//  ProgressBar
//
//  Created by Akkarin Phain on 30/4/2564 BE.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var progressBar: ProgressBar!
    
    var count: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        count = 0
        setupProgressBar(count: count)
    }
    

    @IBAction func minusButtonAction(_ sender: Any) {
        count -= 1
        setupProgressBar(count: count)
    }
    @IBAction func plusButtonAction(_ sender: Any) {
        count += 1
        setupProgressBar(count: count)
    }
    
    private func setupProgressBar (count: CGFloat) {
        progressBar.progress = min(0.05 * count, 1)
        
        switch progressBar.progress {
        case 0:
            minusButton.isEnabled = false
            plusButton.isEnabled = true
        case 1:
            minusButton.isEnabled = true
            plusButton.isEnabled = false
            routeTostarPatternViewController()
        default:
            minusButton.isEnabled = true
            plusButton.isEnabled = true
        }

    }
    
    private func setupView() {
        minusButton.layer.cornerRadius = self.minusButton.frame.height / 2
        minusButton.backgroundColor = .blue
        plusButton.layer.cornerRadius = self.minusButton.frame.height / 2
        plusButton.backgroundColor = .blue
        
    }
    
    private func routeTostarPatternViewController() {
        let viewcontroller = self.storyboard?
            .instantiateViewController(withIdentifier: "StarPatternViewController") as! StarPatternViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
}

